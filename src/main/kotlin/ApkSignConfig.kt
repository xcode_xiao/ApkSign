/**
 * 配置
 */
class ApkSignConfig
{
    var buildToolPath = "${System.getenv()["ANDROID_HOME"]}/build-tools/29.0.3"
    var jksPath = "app/sign.jks"
    var jksPasswd = "test"
    var alias = "test"
    var aliasPass = "test"
    var unSignApkFile = "Downloads/unsign.apk"
    var signApkDir = "User/Downloads"
}